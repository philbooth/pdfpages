# Changes

## 0.1.0

* feature: implement basic pdf-extraction script (bcf485d)
* feature: add a readme and license (09b6c45)
* chore: add requirements file (b695e5d)
* refactor: break apart the arg parsing from the pdf stuff (cb1979e)
* feature: use setuptools to define package info (1e7c318)

## 0.0.0

Initial version.

